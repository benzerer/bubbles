export function random (floor = 0, limit = 100) {
	let result = Math.random() * limit * 100;
	result = Math.floor(result) / 100;
	result += floor;
	result = Math.min(result, limit);
	return result;
}